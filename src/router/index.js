import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import RegisterView from '../views/RegisterView.vue'
import GenerateQRView from '../views/GenerateQRView.vue'
import QrView from '../views/QrView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/register',
    name: 'register',
    component: RegisterView
  },
  {
    path: '/qr',
    name: 'qr',
    component: QrView
  },
  {
    path: '/generate',
    name: 'generate',
    component: GenerateQRView
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
