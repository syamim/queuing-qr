import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './assets/index.css'

// firebase setup
// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
// https://firebase.google.com/docs/web/setup#available-libraries

// Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyC8_tIcV-qi47oYq2AAwHIZCyJzLp3m4nw",
  authDomain: "queing-qr.firebaseapp.com",
  projectId: "queing-qr",
  storageBucket: "queing-qr.appspot.com",
  messagingSenderId: "265631314366",
  appId: "1:265631314366:web:474bb7dc0d9803b31789ef"
};

// Initialize Firebase
const firebaseApp = initializeApp(firebaseConfig)
// Initialize Firebase Authentication and get a reference to the service
const auth = getAuth(firebaseApp)
// Initialize Firestore
const db = getFirestore(firebaseApp)
// end firebase setup

const app = createApp(App)
window.db =  db;
window.auth =  auth;
app.use(store)
app.use(router)
app.mount('#app')
